#!/bin/sh

getmd5sum () {
	md5sum "$1" | sed 's/[^0-9a-z].*$//'
}

appentry () {
	name=$1
	if [ "$name" = "shared" ]; then
		path=../shared
	else
		path=$name
	fi
	cat <<END
{
  "name": "$name",
  "manifest_url": "http://$name.localhost/manifest.webmanifest",
  "install_time": $(date +%s)000,
  "manifest_hash": "$(getmd5sum "$path/manifest.webmanifest")"
},
END
}

echo "["
cd apps
for app in * shared; do
	appentry "$app"
done | sed -z 's/,[^,]*$//'
echo
echo "]"
