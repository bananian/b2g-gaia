user_pref("b2g.system_startup_url", "chrome://system/content/index.html");
user_pref("b2g.multiscreen.system_remote_url", "chrome://system/content/index.html");
// Tell Gecko to use Wayland's input method instead of B2G's
user_pref("dom.inputmethod.enabled", false);
