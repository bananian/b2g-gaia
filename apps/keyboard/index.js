let preventedKeys = new Set([
  'ArrowLeft', 'ArrowRight',
  '0','1','2','3','4','5','6','7','8','9','*','#'
]);

window.onload = window.onhashchange = () => {
  switch(location.hash){
    case '#hasvalue':
      preventedKeys.add('Backspace');
      break;
    default:
      preventedKeys.delete('Backspace');
      break;
  }
};

window.onkeydown = window.onkeyup = (e) => {
  if(preventedKeys.has(e.key)){
    e.preventDefault();
    e.stopPropagation();
  }
};
