const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const DefinePlugin = require('webpack').DefinePlugin;

module.exports = {
	entry: {
		system: './src/singletab.js',
		neterror: './src/neterror.js'
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].bundle.js'
	},
	devtool: 'source-map',
	plugins: [
		new CopyPlugin({
			patterns: [
				'manifest.webmanifest',
				'index.html',
				'net_error.html',
				{from: 'src/styles', to: 'styles'}
			]
		}),
		new DefinePlugin({
			GAIA_DOMAIN: '"' + process.env.GAIA_DOMAIN + '"',
			API_DAEMON_HOST: '"' + process.env.API_DAEMON_HOST + '"'
		})
	],
	mode: 'development'
};
