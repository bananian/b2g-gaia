const inputBlockedKeys = [
  "0","1","2","3","4","5","6","7","8","9","*","#",
  "Process",
  "Backspace",
  "ArrowLeft",
  "ArrowRight"
];

export default class KeyHandler {
  handleEvent(e) {
    const prevented = e.defaultPrevented;
    switch(`${e.type}-${e.key}`){
      case "keydown-Backspace":
        //console.log(`-*- System: Backspace prevented: ${prevented}`);
        if(prevented) break;
        console.log(`-*- System: app closed by key`);
        window.dispatchEvent(new CustomEvent('windowclose'));
        break;
    }
  }
  start() {
    this._block = false;
    navigator.b2g.setDispatchKeyToContentFirst(true);
    window.addEventListener('keydown', this);
  }
  stop() {
    window.removeEventListener('keydown', this);
  }
}
