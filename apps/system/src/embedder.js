function dbg(msg){
  console.log(`-*- Embedder: ${msg}`);
}

export default class Embedder {
  constructor(){
    this.we = new WebEmbedder({
      windowProvider: {
        openURI: this.w_openURI,
        createContentWindow: this.w_createCW,
        openURIInFrame: this.w_openURIInFrame,
        createContentWindowInFrame: this.w_createCWInFrame,
        isTabContentWindow: this.w_isTabCW,
        canClose: this.w_canClose,
      },
      notifications: {
        //showNotification: this.n_show,
        //closeNotification: this.n_close,
      },
      // imeHandler
      // screenReaderProvider
      activityChooser: {
        //choseActivity: this.a_chose,
      },
      processSelector: {
        provideProcess: this.p_provide,
        suggestServiceWorkerProcess: this.p_suggestSW,
      },
    });
  }
  start() {
    this.we.addEventListener('launch-app', this.launchApp);
    //this.we.addEventListener('runtime-ready', () => {});
  }
  openChrome = (params, uri) => {
    let chromeWnd = (window.openDialog || window.open)(
      `chrome://b2g/content/shell.html`,
      '_blank',
      'chrome,dialog=no,non-private,remote',
      'secondary_window',
      uri,
      !!params && !!params.features &&
      params.features.split(',').includes('kind=app')
    );
    return {
      frame: chromeWnd, /* TODO: extract real frame from system app */
    };
  }
  launchApp = (e) => {
    this.openChrome({
      features: 'noopener,kind=app'
    }, e.detail.manifestUrl);
  }
  w_openURI = (uri, openwindowinfo, where, flags, tp, csp) => {
    throw new Error('NOT IMPLEMENTED');
  }
  w_createCW = (uri, openwindowinfo, where, flags, tp, csp) => {
    throw new Error('NOT IMPLEMENTED');
  }
  w_openURIInFrame = (uri, params, where, flags, nextremotetab, name) => {
    dbg(`openURIInFrame ${uri} ${name}`);
    return this.openChrome(params, uri);
  }
  w_createCWInFrame = (uri, params, where, flags, nextremotetab, name) => {
    dbg(`createCWInFrame ${uri} ${name}`);
    return this.openChrome(params, uri);
  }
  w_isTabCW = (win) => false
  w_canClose = (win) => true
  p_provide = (type, processes, maxcount) => -1
  p_suggestSW = (scope) => 0
}
