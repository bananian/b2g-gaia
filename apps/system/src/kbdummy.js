export default class KeyboardDummy {
  constructor() {
    this.wv = document.createElement('web-view');
    this.wv.style.display = 'none';
    this.wv.openWindowInfo = null;
    this.wv.setAttribute('remote', true);
    this.wv.setAttribute('transparent', true);
    this.wv.setAttribute('ignoreuserfocus', true);
    this.setMode('disabled');
    this._active = false;
    document.body.appendChild(this.wv);
  }
  sendValue(value) {
    this.setMode(value ? 'hasvalue' : 'emptyvalue');
    if(this._active) return;
    console.log('Activating keyboard');
    this._active = true;
    this.wv.activateKeyForwarding();
  }
  deactivate() {
    this.setMode('disabled');
    if(!this._active) return;
    console.log('Deactivating keyboard');
    this._active = false;
    this.wv.deactivateKeyForwarding();
  }
  setMode(mode) {
    console.log(`Setting keyboard mode to ${mode}`);
    this.wv.src = `http://keyboard.${GAIA_DOMAIN}/index.html#${mode}`;
  }
}
