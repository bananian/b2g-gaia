import KeyHandler from './keyhandler.js';
import KeyboardDummy from './kbdummy.js';
import Embedder from './embedder.js';

window.SERVICES = {
  keyhandler: new KeyHandler(),
  kbdummy: new KeyboardDummy(),
  embedder: new Embedder(),
};

let wv = document.createElement('web-view');
wv.openWindowInfo = null;
wv.setAttribute('remote', 'true');
let handlers = {
  'close': () => {
    console.log(`-*- System: app closed`);
    window.dispatchEvent(new CustomEvent('windowclose'));
  },
  'inputstatechange': (e) => {
    if(e.detail.editing){
      SERVICES.kbdummy.sendValue(e.detail.value);
    }
    else {
      SERVICES.kbdummy.deactivate();
    }
  },
  'titlechange': (e) => {
    console.log(`-*- System: title change`, e.detail);
  },
  'securitychange': (e) => {
    console.log(`-*- System: security change`, e.detail);
  },
  'locationchange': (e) => {
    console.log(`-*- System: location change`, e.detail);
  },
  'manifestchange': (e) => {
    console.log(`-*- System: manifest change`, e.detail);
  },
  'showmodalprompt': (e) => {
    console.log(`-*- System: prompt`, e.detail);
    e.detail.unblock();
  },
  'backgroundcolor': (e) => {
    console.log(`-*- System: background color`, e.detail);
  },
  'loadstart': () => {
  },
  'loadend': () => {
  },
  'processready': () => {
  }
};
for(const [ key, handler ] of Object.entries(handlers)){
  wv.addEventListener(key, handler);
}
document.body.appendChild(wv);
wv.focus();
window.addEventListener('message', (e) => {
  if(e.data.manifestURL){
    console.log(`-*- System: received manifestURL ${e.data.manifestURL}`);
    fetch(e.data.manifestURL)
      .then((req) => req.json())
      .then((json) => {
        let url = new URL(json.start_url || "/", e.data.manifestURL);
        wv.setAttribute('src', url.href);
      })
      .catch((e) => {
        console.log('-*- System: manifest load error', e);
      });
  }
  else if(e.data.url){
    console.log(`-*- System: received webpage URL ${e.data.url}`);
    wv.setAttribute('src', e.data.url);
    document.body.appendChild(wv);
    wv.focus();
  }
});
SERVICES.keyhandler.start();
SERVICES.embedder.start();
