WEBAPPSDIR=$(CURDIR)/profile/webapps
BUILDVARS = API_DAEMON_HOST GAIA_DOMAIN DESKTOP PRODUCTION
BUILDVARS_MK = $(CURDIR)/buildvars.mk

# Default configuration
API_DAEMON_HOST=127.0.0.1
DESKTOP=0
GAIA_DOMAIN=localhost
ifeq ($(DESKTOP),1)
API_DAEMON_HOST=127.0.0.1:8081
GAIA_DOMAIN=localhost:8081
endif

ifeq ($(MAKECMDGOALS), production)
PRODUCTION=1
else
PRODUCTION=0
endif

export $(BUILDVARS) BUILDVARS_MK

all: profile

.PHONY: production
production: profile

profile: build-apps
	mkdir -p "$(WEBAPPSDIR)"
	cd apps && set -e && for app in *; do \
		mkdir -p "$(WEBAPPSDIR)/$$app" && \
		cp "$$app/application.zip" "$(WEBAPPSDIR)/$$app" && \
		cp "$$app/manifest.webmanifest" "$(WEBAPPSDIR)/$$app"; \
	done
	mkdir -p "$(WEBAPPSDIR)/shared"
	cp shared/application.zip "$(WEBAPPSDIR)/shared"
	cp shared/manifest.webmanifest "$(WEBAPPSDIR)/shared"
	./generate-webapps-json.sh > profile/webapps/webapps.json
	cp prefs.js profile/prefs.js

.PHONY: build-apps
build-apps: buildvars.mk
	for app in apps/* shared; do \
		make -C "$$app"; \
	done

.PHONY: clean
clean:
	for app in apps/* shared; do \
		make -C "$$app" clean; \
	done
	rm -rf .buildvars buildvars.mk

buildvars.mk: buildvars
	@echo 'Generating buildvars.mk...'
	@echo '## AUTO GENERATED. DO NOT EDIT.' > $@
	@for var in $(BUILDVARS); do \
		echo "dep-$$var="'$(CURDIR)/.buildvars/'"$$var" >> '$@'; \
	done

.PHONY: buildvars
buildvars:
	@mkdir -p .buildvars
	@for var in $(BUILDVARS); do \
		if [ ! -f ".buildvars/$$var" ] || [ "x$$(cat .buildvars/$$var)"\
			!= "x$$(eval "echo \$$$$var")" ]; \
		then \
			eval "echo \$${$$var}" > .buildvars/$$var; \
			echo "Updated $$var"; \
		fi; \
	done
